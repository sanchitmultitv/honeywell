import { Component, OnInit, OnDestroy } from '@angular/core';
import { FetchDataService } from '../../services/fetch-data.service';
import { FormControl } from '@angular/forms';
import { ChatService } from 'src/app/services/chat.service';
import { Router } from '@angular/router';

declare var $: any;
@Component({
  selector: 'app-chats',
  templateUrl: './chats.component.html',
  styleUrls: ['./chats.component.scss']
})
export class ChatsComponent implements OnInit, OnDestroy {
  commentsList = [];
  commentsListing = [];
  textMessage = new FormControl('');
  type = new FormControl('');
  interval;
  typ = 'normal';
  chatMessage = [];
  oneToOneChatList = [];
  allChatList = [];
  searchChatList = [];
  allChatIndex = 0;
  sender_id: any;
  sender_name;
  receiver_id: any;
  receiver_name;
  constructor(public router: Router,private _fd: FetchDataService, private chat: ChatService) { }

  ngOnInit(): void {
    
    this.getAllAttendees();
    this.chat.getconnect('toujeo-60');
    // this.chat.getMessages().subscribe((data=>{
    //     let getMsg = data.split('_');
    //     console.log('chats',getMsg);
    //     if(getMsg[0] == "one" && getMsg[1] == "to" && getMsg[2] == "one"){
    //       let data = JSON.parse(localStorage.getItem('virtual'));
    //       if(getMsg[3]== data.id){
    //         // this.ChatMsg = true;
    //         this._fd.enterTochatList(this.receiver_id, this.sender_id).subscribe(res => {
    //           this.chatMessage = res.result;
    //         });
    //       }
    //     }
        
    // }))

    this.chat.getMessages().subscribe((data=>{
      // alert(data)
      if(data === 'start_audio'){
        this.router.navigate(['/auditorium/one']);
      }
      let check = data.split('_');
      console.log(data,'okkk');
      let datas = JSON.parse(localStorage.getItem('virtual'));
      if(check[0] == 'one2one' && check[1]== datas.id) {
         //alert(data);
         // this.getQA();
         this._fd.enterTochatList(this.receiver_id, this.sender_id).subscribe(res => {
                     this.chatMessage = res.result;
                 });
        }
      }));

    
    
  }
  

  getAllAttendees() {
    let event_id = 60;
    this.sender_id = JSON.parse(localStorage.getItem('virtual')).id;
    this.sender_name = JSON.parse(localStorage.getItem('virtual')).name;
    this._fd.getAttendees(event_id).subscribe((res: any) => {
      this.allChatList = res.result;
      this.searchChatList = res.result;
      this.receiver_id = res.result[0].id;
      this.receiver_name = res.result[0].name;
      this._fd.enterTochatList(this.receiver_id, this.sender_id).subscribe(res => {
        this.chatMessage = res.result;
      });

      this.timer = setInterval(() => {
            this._fd.enterTochatList(this.receiver_id, this.sender_id).subscribe(res => {
              this.chatMessage = res.result;
            });

          }, 150000);
      this._fd.enterTochatList(this.receiver_id, this.sender_id).subscribe(res => {
            this.chatMessage = res.result;
          });

    });
  }
  searchElement(query) {
    let event_id = 60;
    this._fd.getOne2oneChatList(event_id, query).subscribe(res => {
      this.allChatList = res.result;
    });
  }

  selectedChat(chat, ind) {
    this.allChatIndex = ind;
    let event_id = 60;
    this.sender_id = JSON.parse(localStorage.getItem('virtual')).id;
    this.sender_name = JSON.parse(localStorage.getItem('virtual')).name;
    this.receiver_id = chat.id;
    this.receiver_name = chat.name;
    console.log(chat)
    this._fd.enterTochatList(this.receiver_id, this.sender_id).subscribe(res => {
      this.chatMessage = res.result;
    });
  }
  timer;
  postOneToOneChat(event) {
    let msg = event.value;
    const formData = new FormData();
    formData.append('sender_id', this.sender_id);
    formData.append('sender_name', this.sender_name);
    formData.append('receiver_id', this.receiver_id);
    formData.append('receiver_name', this.receiver_name);
    formData.append('msg', msg);
    if (event.value !== null){
      this._fd.postOne2oneChat(formData).subscribe(data => {
        this.textMessage.reset();
        this._fd.enterTochatList(this.receiver_id, this.sender_id).subscribe(res => {
          this.chatMessage = res.result;

        });
      });
    }
    setTimeout(() => {
      $('#chat_messaged')[0].scrollTop = $('#chat_messaged')[0].scrollHeight-100;
    }, 1500);
    // this.timer = setInterval(() => {
    //   this._fd.enterTochatList(this.receiver_id, this.sender_id).subscribe(res => {
    //     this.chatMessage = res.result;
    //   });
    // }, 1000);
  }

  getComments() {
    let event_id = 53;
    let user_id = 1;
    let type = 'normal';
    this._fd.getComments(event_id, user_id, type).subscribe(res => {
      this.commentsList = res.result;
      this.commentsListing = res.result;
    });
  }
  getType(value) {
    console.log(value);
    let event_id = 53;
    let user_id = 1;
    this.typ = value;
    this._fd.getComments(event_id, user_id, value).subscribe(res => {
      this.commentsList = res.result;
    })
  }
  closePopup() {
    $('.chatsModal').modal('hide');
  }

  // postComment(text) {
  //   let data: any = JSON.parse(localStorage.getItem('virtual'));
  //   const formData = new FormData();
  //   formData.append('event_id', '123');
  //   formData.append('user_id', '1');
  //   formData.append('name', data.name);
  //   formData.append('comment', text);
  //   formData.append('type', this.typ);
  //   this._fd.postComments(formData).subscribe(res => {
  //     console.log(res);
  //   })
  //   this.textMessage.reset();
  // }

  ngOnDestroy() {
    //clearInterval(this.interval);
   // clearInterval(this.timer);
  }
}
