import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-audio-screen',
  templateUrl: './audio-screen.component.html',
  styleUrls: ['./audio-screen.component.scss']
})
export class AudioScreenComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit(): void {
    this.playAudio();
  }
  playAudio() {
    localStorage.setItem('play', 'play');
    let abc: any = document.getElementById('myAudio');
    abc.play();
    // alert('after login audio')
  }
  naviTolob() {
    this.router.navigate(['/lobby']);
  }
}
