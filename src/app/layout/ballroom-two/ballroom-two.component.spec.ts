import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BallroomTwoComponent } from './ballroom-two.component';

describe('BallroomTwoComponent', () => {
  let component: BallroomTwoComponent;
  let fixture: ComponentFixture<BallroomTwoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BallroomTwoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BallroomTwoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
