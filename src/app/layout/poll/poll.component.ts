import { Component, OnInit } from '@angular/core';
import { ChatService } from 'src/app/services/chat.service';
import { FetchDataService } from 'src/app/services/fetch-data.service';
declare var $: any;
@Component({
  selector: 'app-poll',
  templateUrl: './poll.component.html',
  styleUrls: ['./poll.component.scss']
})
export class PollComponent implements OnInit {
  quizData: any = [];
  quizlist = [];
  summaryList = [];
  summaryBoolean = false;
  index = 0;
  sumIndex = 0;
  user_id: any;
  quizStatus = 'Poll';
  correctAnsCount = 0;
  check:any;
  liveMsg= false;
  constructor(private _fd: FetchDataService, private chat: ChatService) { }

  ngOnInit(): void {
    this.chat.getconnect('toujeo-177');
    this.chat.getMessages().subscribe((data => {
      console.log('data',data);
      this.check = data.split('_');
      // alert(this.check)
      if (this.check[0] == 'start' && this.check[1] == 'quiz') {
        // alert(this.check[2])
        let event_id = 177;
        // this.summaryBoolean = false;
        this._fd.getQuizList(event_id, this.check[2]).subscribe((res: any) => {
          this.quizlist = res.result;

          $('.pollModal').modal('show');

          console.log('dddd', res)
        });
      }

      // if(data == 'start_quiz'){
      //   this.liveMsg = true;
      //   $('.quizModal').modal('show');
      // }
      // if(data == 'stop_quiz'){
      //   this.liveMsg = false;
      // }
    }))
   this.user_id = JSON.parse(localStorage.getItem('virtual')).id;

  //  this.getQuizlist();
  //   this.loadData();

  }
  getQuizlist() {
    let event_id = 177;
    this._fd.getQuizList(event_id,this.check[2]).subscribe((res: any) => {
      this.quizlist = res.result;
    });
  }

  loadData() {
    this._fd.getQuiz().subscribe((data: any) => {
      this.quizData = data.results;
      for (let i = 0; i < this.quizData.length; i++) {
        for (let j = 0; j < this.quizData[i]['incorrect_answers'].length; j++) {
          this.quizData[i].option = this.quizData[i]['incorrect_answers'];
        }
        this.quizData[i].option.push(this.quizData[i]['correct_answer']);
        // console.log('time', this.quizData[i])
      }
    });
  }

  selectedOption;
  optionIndex = -1;
  color;
  noColor = '#27272d';
  nextQuestion(index, j, quiz, option) {
    let quiz_id: any = quiz.id;
    let answer: any = quiz.correct_answer;
    this.optionIndex = j;
    this.color = 'green';
    const formData = new FormData();
    formData.append('user_id', this.user_id);
    formData.append('quiz_id', quiz_id);
    formData.append('answer', option);
    this._fd.postSubmitQuiz(formData).subscribe(res => {
      console.log('ind', index, this.quizlist.length);
    });
    setTimeout(() => {
      
      this.index = index +1;
      this.optionIndex = -1;
      this.color = '#27272d';
      if (this.quizlist.length === this.index) {
        let event_id = 177;
        if(index<this.index){
          $('.pollModal').modal('hide');}
        this._fd.getQuizList(event_id, this.check[2]).subscribe((resp:any) => {
          this.summaryBoolean = false;
          this.summaryList = resp.result;
          for (let i = 0; i < this.summaryList.length; i++) {
            if (this.summaryList[i]['correct_answer'] === this.summaryList[i]['your_answer']) {
              this.correctAnsCount = this.correctAnsCount + 1;
            }
            if(index<this.index){
             
              this.index=0
              this.quizlist=[]

            }

          }
          // this.quizStatus = 'Quiz Summary';
         
          $('.pollModal').modal('hide');

         
        });
       
      }
    
    }, 1000);
   
  }

  closePopup() {
    $('.pollModal').modal('hide');
  }
}