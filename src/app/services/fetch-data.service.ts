import { Injectable } from '@angular/core';
import { Login } from './module/login';
import { Observable } from 'rxjs';
import { HttpHeaders, HttpParams, HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { ApiConstants } from './apiConfig/api.constants';

@Injectable({
  providedIn: 'root'
})
export class FetchDataService {
  baseUrl = environment.baseUrl;
  loginBaseUrl = environment.url;
  login = 'RecognizerApi/index.php/api/engine/authentication';


  constructor(private http: HttpClient) { }
  authLogin(user: Login): Observable<any> {
    const headers = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded;charset=utf-8');
    let params = new HttpParams();
    params = params.set('email', user.email);
    params = params.set('password', user.password);
    return this.http.post(`${this.loginBaseUrl}/${this.login}`, params);
  }

  getProfile(id, role_id): Observable<any> {
    return this.http.get(`${this.baseUrl}/${ApiConstants.profile}/id/${id}/role_id/${role_id}`);
  }
  getAttendees(event_id): Observable<any> {
    return this.http.get(`${this.baseUrl}/${ApiConstants.attendees}/${event_id}`);
  }
  getAttendeesbyName(event_id, name): Observable<any> {
    return this.http.get(`${this.baseUrl}/${ApiConstants.attendees}/${event_id}/name/${name}`);
  }
  getComments(event_id, user_id, typ): Observable<any> {
    return this.http.get(`${this.baseUrl}/${ApiConstants.commentsList}/event_id/${event_id}/type/${typ}`);
  }
  postComments(comment: any): Observable<any> {
    return this.http.post(`${this.baseUrl}/${ApiConstants.addComment}`, comment);
  }
  getOne2oneChatList(event_id, name): Observable<any> {
    return this.http.get(`${this.baseUrl}/${ApiConstants.chatAttendeesComments}/event_id/${event_id}/name/${name}`);
  }
  enterTochatList(receiver_id, sender_id): Observable<any> {
    return this.http.get(`${this.baseUrl}/${ApiConstants.One2OneCommentList}/receiver_id/${receiver_id}/sender_id/${sender_id}`)
  }
  postOne2oneChat(comment: any): Observable<any> {
    return this.http.post(`${this.baseUrl}/${ApiConstants.one2oneCommentPost}`, comment);
  }
  getRating(): Observable<any> {
    return this.http.get(`${this.baseUrl}/virtualapi/v1/rating/master/list/event_id/60`);
  }
  getFeedback(): Observable<any> {
    return this.http.get(`${this.baseUrl}/virtualapi/v1/feedback/master/list/event_id/60`);
  }
  postFeedback(feedback: any): Observable<any> {
    return this.http.post(`${this.baseUrl}/virtualapi/v1/feedback/post`, feedback);
  }
  postRating(rating: any): Observable<any> {
    return this.http.post(`${this.baseUrl}/virtualapi/v1/rating/post`, rating);
  }
  getExhibition(title): Observable<any> {
    return this.http.get(`${this.baseUrl}/${ApiConstants.exhibition}/${title}`);
  }
  getQuiz() {
    return this.http.get('https://opentdb.com/api.php?amount=50&category=11&type=multiple');
  }

  askQuestions(id,name, value,exhibit_id, exhibit_name,status): Observable<any> {
    const headers = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded;charset=utf-8');
    let params = new HttpParams();
    params = params.set('sender_id', id);
    params = params.set('sender_name', name);
    params = params.set('receiver_id', exhibit_id);
    params = params.set('receiver_name', exhibit_name);
    params = params.set('msg', value);
    params = params.set('status', status);
    return this.http.post(`${this.baseUrl}/${ApiConstants.askQuestion}`, params);
  }
  helpdesk(id,value): Observable<any> {
    const headers = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded;charset=utf-8');
    let params = new HttpParams();
    params = params.set('event_id', '177');
    params = params.set('user_id', id);
    params = params.set('question', value);
    return this.http.post(`${this.baseUrl}/${ApiConstants.helpdesk}`, params);
  }
  gethelpdeskanswers(uid): Observable<any> {
    return this.http.get(`${this.baseUrl}/${ApiConstants.helpdeskAnswes}/${uid}/event_id/177`)
  }
  askLiveQuestions(id, value, audi_id): Observable<any> {
    const headers = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded;charset=utf-8');
    let params = new HttpParams();
    params = params.set('user_id', id);
    params = params.set('question', value);
    params = params.set('event_id', '177');
    params = params.set('audi_id', audi_id);

    return this.http.post(`${this.baseUrl}/${ApiConstants.askQuestionLive}`, params);
  }
  getanswers(uid,exhibit_id): Observable<any> {
    return this.http.get(`${this.baseUrl}/${ApiConstants.getQuestionAnswser}/${uid}/sender_id/${exhibit_id}`)
  }
  // Liveanswers(): Observable<any> {
  //   return this.http.get(`${this.baseUrl}/${ApiConstants.getliveAnswser}`)
  // }
  getPollList(id): Observable<any> {
    return this.http.get(`${this.baseUrl}/${ApiConstants.getPolls}/${id}`);
  }
  // getPollListTwo(id): Observable<any> {
  //   return this.http.get(`${this.baseUrl}/${ApiConstants.getPolls}/${id}`);
  // }
  // getPollListThree(id): Observable<any> {
  //   return this.http.get(`${this.baseUrl}/${ApiConstants.getPolls}/${id}`);
  // }
  // getPollListFour(id): Observable<any> {
  //   return this.http.get(`${this.baseUrl}/${ApiConstants.getPolls}/${id}`);
  // }
  postPoll(id, data, value): Observable<any> {
    const headers = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded;charset=utf-8');
    let params = new HttpParams();
    params = params.set('poll_id', id);
    params = params.set('user_id', data);
    params = params.set('answer', value);
    return this.http.post(`${this.baseUrl}/${ApiConstants.postPolls}`, params);
  }
  getWtsappFiles(): Observable<any> {
    return this.http.get(`${this.baseUrl}/${ApiConstants.wtsappAgendaFiles}`);
  }

  getQuizList(event_id,cat_id): Observable<any> {
    return this.http.get(`${this.baseUrl}/${ApiConstants.quizlist}/event_id/${event_id}/cat_id/${cat_id}`);
  }
  postSubmitQuiz(quiz): Observable<any> {
    return this.http.post(`${this.baseUrl}/${ApiConstants.submitQuiz}`, quiz);
  }
  getSummaryQuiz(event_id, user_id): Observable<any> {
    return this.http.get(`${this.baseUrl}/${ApiConstants.summaryQuiz}/event_id/${event_id}/user_id/${user_id}`);
  }

  uploadCameraPic(pic): Observable<any> {
    return this.http.post(`${this.baseUrl}/virtualapi/v1/upload/pic`, pic);
  }
  uploadsample(pic): Observable<any> {
    // return this.http.post(`https://tbbmedialive.com/Q3PCM2020/uploadblob.php`, pic);
    // return this.http.post(`https://virtualapi.multitvsolution.com/upload_photo/uploadblob.php`, pic);
    return this.http.post(`https://virtualapi.multitvsolution.com/upload_photo/uploadblob.php`, pic);
  }
  groupchating(): Observable<any> {
    return this.http.get(`${this.baseUrl}/${ApiConstants.groupChats}/myanmar_16`);
  }

  postGroupchat(value, name, email, room, created,uid):Observable<any> {
    const headers = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded;charset=utf-8');
    let params = new HttpParams();
    params = params.set('room_name', room);
    params = params.set('user_name', name);
    params = params.set('email', email);
    params = params.set('chat_data', value);
    params = params.set('is_approved', '1');
    params = params.set('created', created);
    params = params.set('user_id', uid);
    return this.http.post(`${this.baseUrl}/virtualapi/v1/groupchat/post/event_id/177`, params)
  }
  groupchatingtwo(): Observable<any> {
    return this.http.get(`${this.baseUrl}/${ApiConstants.groupChats}/myanmar_17`);
  }
  groupchatingthree(): Observable<any> {
    return this.http.get(`${this.baseUrl}/${ApiConstants.groupChats}/myanmar_18`);
  }
  groupchatingfour(): Observable<any> {
    return this.http.get(`${this.baseUrl}/${ApiConstants.groupChats}/myanmar_19`);
  }
  submitWheelScore(wheel: any) {
    return this.http.post(`${this.baseUrl}/virtualapi/v1/user/game/post`, wheel);
  }
  getBriefcaseList(event_id, user_id) {
    return this.http.get(`${this.baseUrl}:7000/${ApiConstants.briefcaseList}/event_id/${event_id}/user_id/${user_id}`);
  }
  postBriefcase(doc: any) {
    return this.http.post(`${this.baseUrl}:7000/${ApiConstants.postBriefcase}`, doc);
  }
  analyticsPost(analytics: any): Observable<any> {
    return this.http.post(`${this.baseUrl}/virtualapi/v1/analytics/user/history/add`, analytics);
  }
  activeAudi(): Observable<any> {
    return this.http.get(`${this.baseUrl}/${ApiConstants.activeInactive}`)
  }
  heartbeat(params): Observable<any> {
    return this.http.post(`${this.baseUrl}/virtualapi/v1/analytics/user/heartbeat`, params);
  }
  enquiryForm(params): Observable<any>{
    return this.http.post(`https://goapi.multitvsolution.com:7000/virtualapi/v1/enquiry/form/add/event_id/177`, params)
  }
  getPlayerUrl(event_id): Observable<any>{
    return this.http.get(`https://goapi.multitvsolution.com:7000/virtualapi/v1/get/auditorium/event_id/${event_id}`);
  }
}
