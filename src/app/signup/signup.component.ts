import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { FetchDataService } from '../services/fetch-data.service';
import { AuthService } from '../services/auth.service';
function emailDomainValidator(control: FormControl) {
  let email = control.value;
  if (email && email.indexOf("@") != -1) {
    let [_, domain] = email.split("@");
    if (domain !== "sanofi.com" && domain !== "sanofi-india.com") {
      return {
        emailDomain: {
          parsedDomain: domain
        }
      }
    }
  }
  return null;
}
@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit {
  checked = false;
  checkMessage = 'Please click on agree to Register';
  signupForm = new FormGroup({
    first_name: new FormControl('', [Validators.required]),
    email: new FormControl('', [Validators.required, Validators.email,
    Validators.pattern("[^ @]*@[^ @]*")]),
    organisation: new FormControl('', [Validators.required]),
    mobile: new FormControl('', [Validators.required]),
  });

  public imagePath;
  imgURL: any;
  public message: string;
  msg;
  colr;
  constructor(private router: Router, private _fd: FetchDataService, private _auth: AuthService) { }

  ngOnInit(): void {

  }

  preview(event) {
    let files = event.target.files;
    if (files.length === 0)
      return;

    var mimeType = files[0].type;
    if (mimeType.match(/image\/*/) == null) {
      this.message = "Only images are supported.";
      return;
    } else {
      const file = files[0];
      this.signupForm.patchValue({
        image: file
      });
    }

    var reader = new FileReader();
    this.imagePath = files;
    reader.readAsDataURL(files[0]);
    reader.onload = (_event) => {
      this.imgURL = reader.result;
    }
  }

  // isChecked(event){
  //   this.checked = !this.checked;
  // }
  register() {
    if (this.signupForm.invalid) {
      return
    } else {
      const formData = new FormData();
      formData.append('name', this.signupForm.get('first_name').value);
      formData.append('email', this.signupForm.get('email').value);
      formData.append('company', this.signupForm.get('organisation').value);
      formData.append('mobile', this.signupForm.get('mobile').value);
      formData.append('category', 'others');
      formData.append('designation', 'others');
      formData.append('type', 'others');
      formData.append('password', 'others');
      // formData.append('token', '123');
      // if (this.checked) {
      this._auth.register(formData).subscribe((res: any) => {

        if (res.code == 1) {
          this.msg = 'Thank You For Registering.';
          setTimeout(() => {
            this.router.navigate(['/thank-you']);
          }, 2000);
        }
        else {
          this.msg = res.result;
        }
        this.signupForm.reset();
      });
      // } else {

      // }
    }
  }
}
