import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule} from '@angular/forms';
import { ExhibitionHallRoutingModule } from './exhibition-hall-routing.module';
import { ExhibitLifeComponent } from './exhibit-life/exhibit-life.component';
import { LifeboyComponent } from './lifeboy/lifeboy.component';
import { VgCoreModule } from 'videogular2/compiled/core';
import { VgControlsModule } from 'videogular2/compiled/controls';
import { VgOverlayPlayModule } from 'videogular2/compiled/overlay-play';
import { VgBufferingModule} from 'videogular2/compiled/buffering';
import { AdaptComponent } from './adapt/adapt.component';
import { AriseComponent } from './arise/arise.component';
import { AchiveComponent } from './achive/achive.component';
import { GrowBySharingComponent } from './grow-by-sharing/grow-by-sharing.component';



@NgModule({
  declarations: [ExhibitLifeComponent, LifeboyComponent, AdaptComponent, AriseComponent, AchiveComponent, GrowBySharingComponent],
  imports: [
    CommonModule,
    ExhibitionHallRoutingModule,
    VgCoreModule,
    VgControlsModule,
    VgBufferingModule,
    VgOverlayPlayModule,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class ExhibitionHallModule { }
