import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { FetchDataService } from 'src/app/services/fetch-data.service';
import { ChatService } from 'src/app/services/chat.service';
import { DomSanitizer } from '@angular/platform-browser';
import { MessageService } from 'src/app/services/message.service';
import { ActivatedRoute } from '@angular/router';
declare var $: any;
import { FormGroup, Validators } from '@angular/forms';
import { any } from 'underscore';

@Component({
  selector: 'app-arise',
  templateUrl: './arise.component.html',
  styleUrls: ['./arise.component.scss']
})
export class AriseComponent implements OnInit {

  exhibiton: any = [];
  documents: any = [];
  textMessage = new FormControl('');
  msg;
  qaList;
  exhibition_id;
  pdf;
  document;
  hidechat = false;
  documentName;
  exhibitionName;
  banner;
  roomName;
  newMessage: string;
  msgs: string;
  newMSg = [];
  myid;
  uName;
  showDiv = true;
  showchat = true;;
  isShow = true;
  videosource;
  status: any;
  c1: any
  c2: any; c3: any; c4: any; c5: any;
  exhibition_title;
  enquiryForm = new FormGroup({
    name: new FormControl('', Validators.required),
    email: new FormControl('', [Validators.required, Validators.email,
    Validators.pattern("[^ @]*@[^ @]*")]),
    company: new FormControl('', Validators.required),
    contact: new FormControl('', Validators.required),
    fire: new FormControl('', Validators.required),
    security: new FormControl('', Validators.required),
    electrical: new FormControl('', Validators.required),
    bms: new FormControl('', Validators.required),
  });
  fires: any;
  electricals: any;
  bmss: any;
  securitys: any;

  constructor(private _fd: FetchDataService, private chat: ChatService, private sanitiser: DomSanitizer, private messageService: MessageService, private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.getExhibit();

    this.getQA();
    let virtual: any = JSON.parse(localStorage.getItem('virtual'));
    this.myid = virtual.id;
    setTimeout(() => {
      this.stepUpAnalytics();
    }, 2000);
    this.chat.getconnect('toujeo-177');
    this.chat.getMessages().subscribe((data => {
      let check = data.split('_');
      console.log(data, 'okkk');
      if (check[0] == 'one2one' && check[1] == this.myid) {
        //alert(data);
        this.getQA();
      }
    }));
    let playVideo: any = document.getElementById('playVideo');
    window.onclick = (event) => {
      if (event.target == playVideo) {
        playVideo.style.display = "none";
        let pauseVideo: any = document.getElementById("video");
        pauseVideo.currentTime = 0;
        pauseVideo.pause();
      }
    }
  }
  stepUpAnalytics() {
    let virtual: any = JSON.parse(localStorage.getItem('virtual'));
    let yyyy: any = new Date().getFullYear();
    let dd: any = new Date().getDate();
    let mm: any = new Date().getMonth() + 1;
    let time: any = new Date().getHours() + ':' + new Date().getMinutes() + ':' + new Date().getSeconds();
    if (dd < 10) {
      dd = '0' + dd;
    }
    if (mm < 10) {
      mm = '0' + mm;
    }
    const formData = new FormData();
    formData.append('event_id', '177');
    formData.append('user_id', virtual.id);
    formData.append('name', virtual.name);
    formData.append('email', virtual.email);
    formData.append('company', 'others');
    formData.append('designation', 'others');
    formData.append('action', 'click_' + this.exhibition_title);
    formData.append('created', yyyy + '-' + mm + '-' + dd + ' ' + time);
    this._fd.analyticsPost(formData).subscribe(res => {
      console.log('asdf', res);
    });
  }
  getUser() {
    let user = JSON.parse(localStorage.getItem('virtual'));
    this.uName = user.name;
    //  console.log(this.uName);
    // let room = JSON.parse(localStorage.getItem('room_id'));
    //console.log(room);
    this.roomName = 'ddbjb1bk';
    this.chat.addUser(this.uName, this.roomName);
  }
  getQA() {
    this.route.paramMap.subscribe(params => {
      this.exhibitionName = params.get('id');
      //  console.log('exhibitonid',this.exhibition_id);
      let data = JSON.parse(localStorage.getItem('virtual'));
      // console.log('uid',data.id);
      this._fd.getanswers(data.id, this.exhibitionName).subscribe((res => {
        //console.log(res);
        this.qaList = res.result;
        // alert('hello');
      }))
    });
  }
  postQuestion(value) {
    // $('.customPost').after('<div  class="card p-2 style="border-left: 5px solid cadetblue; margin-bottom: 5px"><div  id="image_profile" style="padding: 1px 12px;"><span _ class="profileImage card-body  title="karan_bajaj">k</span><div _id="profile_detail"><div>'+value+'</div></div></div></div>');
    this.route.paramMap.subscribe(params => {
      this.exhibitionName = params.get('id');
      let data = JSON.parse(localStorage.getItem('virtual'));
      this.status = 'unread';
      console.log('value', value);
      // this.getQA();
      let arr = {
        'sender_id': data.id,
        'sender_name': data.name,
        'receiver_id': this.exhibitionName,
        'receiver_name': this.exhibition_title,
        'msg': value
      };
      this.qaList.push(arr);

      if (value != undefined) {
        this._fd.askQuestions(data.id, data.name, value, this.exhibitionName, this.exhibiton[0].title, this.status).subscribe((res => {
          if (res.code == 1) {
            this.msg = 'Submitted Succesfully';
            this.textMessage.reset();
            const theElement = document.getElementById('setChat');

            const scrollToBottom = (node) => {
              node.scrollTop = node.scrollHeight;
            }

            scrollToBottom(theElement);
            // setTimeout(() => {

            //   $('#chat_messaged')[0].scrollTop = $('#chat_messaged')[0].scrollHeight-100;
            //   this.msg = '';
            //   alert('ook');
            // }, 2000);
            // $('#chat_messaged')[0].scrollTop = $('#chat_messaged')[0].scrollHeight-100;
            //      var d = $('.chat_message');
            //      d.scrollTop(d.prop("scrollHeight"))
          }
          //  this.getQA();


          // setTimeout(() => {
          //   this.msg = '';
          //   $('.liveQuestionModal').modal('hide');
          // }, 2000);

        }))
      }
    });

  }

  enquiry(data) {
    // alert(this.enquiryForm.get('fire').value)
    if (this.enquiryForm.get('fire').value == true) {
      this.fires = 'fire'
    } else {
      this.fires = '0'
    }
    if (this.enquiryForm.get('security').value == true) {
      this.securitys = 'security'
    } else {
      this.securitys = '0'
    }
    if (this.enquiryForm.get('electrical').value == true) {
      this.electricals = 'electrical'
    } else {
      this.electricals = '0'
    }
    if (this.enquiryForm.get('bms').value == true) {
      this.bmss = 'bms'
    } else {
      this.bmss = '0'
    }
    let cat = this.fires + ',' + this.securitys + ',' + this.electricals + ',' + this.bmss
    const formData = new FormData();
    formData.append('name', this.enquiryForm.get('name').value);
    formData.append('email', this.enquiryForm.get('email').value);
    formData.append('company', this.enquiryForm.get('company').value);
    formData.append('contact', this.enquiryForm.get('contact').value);
    formData.append('category', cat);

    this._fd.enquiryForm(formData).subscribe(res => {
      if (res.code == 1) {
        this.msg = 'Submitted Succesfully';
        $('.enquiryForm').modal('hide');
        data.resetForm();

      }
      console.log(res);
      // setTimeout(() => {
        this.msg = '';
      // }, 1000);
      this.enquiryForm.reset();
      // window.location.reload()
    })
  }
  sendMessage() {
    this.roomName = 'ddbjb1bk';
    if (this.msgs != '') {
      this.chat.sendMessage(this.msgs, this.uName, this.roomName);
      this.msgs = '';
    }
  }
  getExhibit() {

    this.route.paramMap.subscribe(params => {
      this.exhibitionName = params.get('id');
      this._fd.getExhibition(this.exhibitionName).subscribe(res => {
        //console.log('exhibition',res);
        this.exhibiton = res.result;
        this.document = res.result[0].brochure;
        this.exhibition_title = res.result[0].title;
        // alert(this.exhibition_title);
        //this.graphic = res.result[0].graphics;
        this.banner = res.result[0].banner;
        //this.videosource = res.result[0].video;
        this.c3 = res.result[0].creatives.creative1;
        this.c4 = res.result[0].creatives.creative2;
        this.c2 = res.result[0].creatives.creative3;
        this.c1 = res.result[0].creatives.creative4;
        this.c5 = res.result[0].creatives.creative5;
        // this.salesPerson = res.result[0].sales;
        // this.product = res.result[0].product[0].url;
        //this.pord_desc =res.result[0].product[0].desc;
        // this.product = res.result[0].product[0].url
        //console.log(this.banner, 'vauvduyvuduv')
      });
      // this._fd.getBrouchers(this.exhibitionName,data.id).subscribe(res=> {
      //   console.log(res.result);
      //   this.brouchers = res.result;
      // })

    });





    // this._fd.getExhibition('Commit to Suceed').subscribe(res => {
    //   console.log('exhibition', res);
    //   this.exhibiton = res.result[0];
    //   this.documents = res.result[0].document;
    //   this.exhibition_id = res.result[0].id;
    //   localStorage.setItem('exhibitData',JSON.stringify(res.result));
    // });
  }
  getDocument(prof) {
    $('.pdfModal').modal('show');
    // window.open("https://s3.ap-south-1.amazonaws.com/acma.multitvsolution.in/assets/acme/speakers_profiles/Kenichi Ayukawa/Brief Profile - Mr. Kenichi Ayukawa.pdf","viewer"); 
    this.document = this.sanitiser.bypassSecurityTrustResourceUrl(prof);
    this.documentName = 'Commit to Suceed Brochure';
    this.messageService.sendMessage(this.document);
    console.log(prof, 'fulldocss');
    console.log(this.document, 'docs');
  }

  closePopvideos() {
    $('.videosModal').modal('hide');
  }
  closepdf() {
    $('.pdfModal').modal('hide');
  }
  lightbox_open() {
    //this.videoUrl = video;
    let lightBoxVideo: any = document.getElementById('VisaChipCardVideo');
    window.scrollTo(0, 0);
    document.getElementById('light').style.display = 'block';
    document.getElementById('fade').style.display = 'block';
    lightBoxVideo.play();

  }
  lightbox_close() {
    let lightBoxVideo: any = document.getElementById('VisaChipCardVideo');
    document.getElementById('light').style.display = 'none';
    document.getElementById('fade').style.display = 'none';
    lightBoxVideo.pause();
  }
  // getQA() {
  //   console.log('exhibitonid', this.exhibition_id);
  //   let data = JSON.parse(localStorage.getItem('virtual'));
  //   // console.log('uid',data.id);
  //   this._fd.getanswers(data.id, this.exhibition_id).subscribe((res => {
  //     //console.log(res);
  //     this.qaList = res.result;
  //   }))
  // }

  hidediv() {
    this.showDiv = false;
    this.showchat = false;
    this.hidechat = true;
  }
  show() {
    this.showDiv = true;
    this.hidechat = false;
    this.showchat = true;
  }



  // postQuestion(value) {
  //   let data = JSON.parse(localStorage.getItem('virtual'));
  //   //  console.log(value, data.id);
  //   this._fd.askQuestions(data.id, value, this.exhibition_id).subscribe((res => {
  //     //console.log(res);
  //     if (res.code == 1) {
  //       this.msg = 'Submitted Succesfully';
  //       this.getQA();
  //     }
  //     setTimeout(() => {
  //       this.msg = '';
  //       $('.liveQuestionModal').modal('hide');
  //     }, 2000);
  //     this.textMessage.reset();
  //   }))
  // }
  closePopupdocs() {
    $('.docsModal').modal('hide');
  }
  closePopup() {
    $('.salesModal').modal('hide');
    $('.pic5').modal('hide');
    $('.pic6').modal('hide');
    $('.pic7').modal('hide');
    $('.pic8').modal('hide');
    $('.pic9').modal('hide');
    $('.enquiryForm').modal('hide')
  }
  openCallBox() {
    $('.scheduleCallmodal').modal('show');
  }
  closeChat() {
    $('.liveQuestionModal').modal('hide');
  }

  playGrowBySharingVideo(video) {
    this.videosource = video;
    let playVideo: any = document.getElementById("video");
    playVideo.play();
    $('#playVideo').modal('show');
  }
  closeModalVideo() {
    let pauseVideo: any = document.getElementById("video");
    pauseVideo.currentTime = 0;
    pauseVideo.pause();
    $('#playVideo').modal('hide');
  }
}
