import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RegistrationDeskComponent } from './registration-desk.component';


const routes: Routes = [
  {path:'', component:RegistrationDeskComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RegistrationDeskRoutingModule { }
