import { Component, OnInit, OnDestroy } from '@angular/core';
import { fadeAnimation } from '../../../../shared/animation/fade.animation';
import { FormGroup, FormControl, Validators, FormArray } from '@angular/forms';
import { FetchDataService } from '../../../../services/fetch-data.service'
import { EventEmitter } from 'events';
import { ChatService } from '../../../../services/chat.service';
declare var $: any;

@Component({
  selector: 'app-auditorium-one',
  templateUrl: './auditorium-one.component.html',
  styleUrls: ['../auditorimStl/audi.style.scss'],
  animations: [fadeAnimation],

})
export class AuditoriumOneComponent implements OnInit, OnDestroy {
  videoEnd = false;
  videoPlayer = 'https://d17uqpjc0q0ra5.cloudfront.net/abr/smil:session4.smil/playlist.m3u8';
  interval;
  like = true;
  imag: any;

  quizData: any = [];
  quizlist = [];
  summaryList = [];
  summaryBoolean = false;
  index = 0;
  sumIndex = 0;
  user_id: any;
  quizStatus = 'Quiz';
  correctAnsCount = 0;
  stream:any;
  // videoPlayer = '../assets/video/networking_lounge_video.mp4';
  constructor(private chatService: ChatService, private _fd: FetchDataService) { }

  ngOnInit(): void {
    // this.getHeartbeat();
    let event_id = 177;
    // let event_id = 172;
    this._fd.getPlayerUrl(event_id).subscribe((res:any)=>{
      this.stream = res.result[0]['stream'];
      // this.playVideo(stream);
      // alert(this.stream)
    });
    this.interval = setInterval(() => {
      this.getHeartbeat();
    }, 60000);
    console.log(this.videoPlayer, 'this is 1');
    this.loadData();
    this.chatService.getconnect('toujeo-177');
    this.chatService.getMessages().subscribe((data => {
      console.log('data', data);
      if (data === 'start_certificate') {
        $('.feebackModal').modal('show');
      }

      if (data == 'groupchat') {
        this.chatGroup();
      }
      

    }));
    this.user_id = JSON.parse(localStorage.getItem('virtual')).id;
    // this.getQuizlist();
  }
  // getQuizlist() {

  //   let event_id = 177;
  //   this._fd.getQuizList(event_id,cat_id).subscribe((res: any) => {
  //     this.quizlist = res.result;
  //     console.log('dddd', res)
  //   });
  // }
  videoEnded() {
    this.videoEnd = true;
  }
  getHeartbeat() {
    let data = JSON.parse(localStorage.getItem('virtual'));
    const formData = new FormData();
    formData.append('user_id', data.id);
    formData.append('event_id', '177');
    formData.append('audi', '205002');
    this._fd.heartbeat(formData).subscribe(res => {
      console.log(res);
    })
  }

  playAudioClap() {
    let playaudio: any = document.getElementById('myAudioClap');
    playaudio.play();
  }
  playAudioWhistle() {
    let playaudio: any = document.getElementById('myAudioWhistle');
    playaudio.play();
  }
  openGroupChat() {
    $('.groupchatOne').modal('show');
    this.messageList = [];
    this.loadData();
  }


  textMessage = new FormControl('');
  newMessage: string[] = [];
  msgs: string;
  messageList: any = [];
  roomName = 'myanmar_16';
  serdia_room = localStorage.getItem('serdia_room');
  loadData() {
    this.chatGroup();
    this.chatService.getconnect('toujeo-60');
    let data = JSON.parse(localStorage.getItem('virtual'));
    this.chatService.addUser(data.name, this.roomName);
    localStorage.setItem('username', data.name);
    this.chatService.receiveMessages(this.roomName).subscribe((msgs: any) => {
      if (msgs.roomId === 1) {
        this.messageList.push(msgs);
      }
      console.log('demo', this.messageList);
    });
    this._fd.getQuiz().subscribe((data: any) => {
      this.quizData = data.results;
      for (let i = 0; i < this.quizData.length; i++) {
        for (let j = 0; j < this.quizData[i]['incorrect_answers'].length; j++) {
          this.quizData[i].option = this.quizData[i]['incorrect_answers'];
        }
        this.quizData[i].option.push(this.quizData[i]['correct_answer']);
        // console.log('time', this.quizData[i])          
      }
    });
  }
  chatGroup() {
    this._fd.groupchating().subscribe(res => {
      console.log('groupChat', res);
      this.messageList = res.result;
    });
  }

  closePopup() {
    $('.groupchatOne').modal('hide');
  }
  postMessage(value) {
    let data = JSON.parse(localStorage.getItem('virtual'));
    let yyyy: any = new Date().getFullYear();
    let dd: any = new Date().getDate();
    let mm: any = new Date().getMonth() + 1;
    let time: any = new Date().getHours() + ':' + new Date().getMinutes() + ':' + new Date().getSeconds();
    if (dd < 10) {
      dd = '0' + dd;
    }
    if (mm < 10) {
      mm = '0' + mm;
    }
    let created = yyyy + '-' + mm + '-' + dd + ' ' + time;
    //  this.chatService.sendMessage(value, data.name, this.roomName);
    this._fd.postGroupchat(value, data.name, data.email, this.roomName, created, data.id).subscribe(res => {
      console.log(res);
    })



    this.textMessage.reset();
    // this.chatGroup();
    //this.newMessage.push(this.msgs);
  }

  selectedOption;
  optionIndex = -1;
  color;
  noColor = '#27272d';
  nextQuestion(index, j, quiz) {
    let quiz_id: any = quiz.id;
    let answer: any = quiz.correct_answer;
    this.optionIndex = j;
    this.color = 'green';
    const formData = new FormData();
    formData.append('user_id', this.user_id);
    formData.append('quiz_id', quiz_id);
    formData.append('answer', answer);
    this._fd.postSubmitQuiz(formData).subscribe(res => {
      console.log('ind', index, this.quizlist.length);
    });
    setTimeout(() => {
      this.index = index + 1;
      this.optionIndex = -1;
      this.color = '#27272d';
      if (this.quizlist.length === this.index) {
        let event_id = 177;
        this._fd.getSummaryQuiz(event_id, this.user_id).subscribe(resp => {
          this.summaryBoolean = true;
          this.summaryList = resp.result;
          this.quizStatus = 'Quiz Summary';
        });
      }
    }, 1000);
  }
  likeopen(data) {
    if (data == 'like') {
      this.imag = 'assets/honeywell/like.png'
    }
    if (data == 'claps') {
      this.imag = 'assets/honeywell/claps.png'
    }
    if (data == 'heart') {
      this.imag = 'assets/honeywell/heart.png'
    }
    this.like = true;
    setTimeout(() => {
      this.like = false;
    }, 10000);
  }
  ngOnDestroy() {
    clearInterval(this.interval);
    // this.chatService.disconnect();
  }
}

