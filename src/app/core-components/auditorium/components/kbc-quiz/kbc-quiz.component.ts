import { Component, OnInit } from '@angular/core';
import { FetchDataService } from '../../../../services/fetch-data.service';
import { ChatService } from 'src/app/services/chat.service';
declare var $: any;

@Component({
  selector: 'app-kbc-quiz',
  templateUrl: './kbc-quiz.component.html',
  styleUrls: ['./kbc-quiz.component.scss']
})
export class KbcQuizComponent implements OnInit {
  quizData: any = [];
  quizlist = [];
  summaryList = [];
  summaryBoolean = false;
  index = 0;
  sumIndex = 0;
  user_id: any;
  quizStatus = 'Quiz';
  correctAnsCount = 0;
  check:any;
  liveMsg= false;
  constructor(private _fd: FetchDataService, private chat: ChatService) { }

  ngOnInit(): void {
    this.chat.getconnect('toujeo-177');
    this.chat.getMessages().subscribe((data => {
      console.log('data',data);
      this.check = data.split('_');
      alert(this.check)
      if (this.check[0] == 'start') {
        alert(this.check[2])
        let event_id = 177;
        this._fd.getQuizList(event_id, this.check[2]).subscribe((res: any) => {
          this.quizlist = res.result;

          $('.quizModal').modal('show');

          console.log('dddd', res)
        });
      }

      // if(data == 'start_quiz'){
      //   this.liveMsg = true;
      //   $('.quizModal').modal('show');
      // }
      // if(data == 'stop_quiz'){
      //   this.liveMsg = false;
      // }
    }))
   this.user_id = JSON.parse(localStorage.getItem('virtual')).id;

   this.getQuizlist();
    this.loadData();

  }
  getQuizlist() {
    let event_id = 52;
    this._fd.getQuizList(event_id,this.check[2]).subscribe((res: any) => {
      this.quizlist = res.result;
    });
  }

  loadData() {
    this._fd.getQuiz().subscribe((data: any) => {
      this.quizData = data.results;
      for (let i = 0; i < this.quizData.length; i++) {
        for (let j = 0; j < this.quizData[i]['incorrect_answers'].length; j++) {
          this.quizData[i].option = this.quizData[i]['incorrect_answers'];
        }
        this.quizData[i].option.push(this.quizData[i]['correct_answer']);
        // console.log('time', this.quizData[i])
      }
    });
  }

  selectedOption;
  optionIndex = -1;
  color;
  noColor = '#27272d';
  nextQuestion(index, j, quiz, option) {
    let quiz_id: any = quiz.id;
    let answer: any = quiz.correct_answer;
    this.optionIndex = j;
    this.color = 'green';
    const formData = new FormData();
    formData.append('user_id', this.user_id);
    formData.append('quiz_id', quiz_id);
    formData.append('answer', option);
    this._fd.postSubmitQuiz(formData).subscribe(res => {
      console.log('ind', index, this.quizlist.length);
    });
    setTimeout(() => {
      this.index = index + 1;
      this.optionIndex = -1;
      this.color = '#27272d';
      if (this.quizlist.length === this.index) {
        let event_id = 177;
       
        this._fd.getSummaryQuiz(177, this.user_id).subscribe((resp:any) => {
          this.summaryBoolean = true;
          this.summaryList = resp.result;
          for (let i = 0; i < this.summaryList.length; i++) {
            if (this.summaryList[i]['correct_answer'] === this.summaryList[i]['your_answer']) {
              this.correctAnsCount = this.correctAnsCount + 1;
            }

          }
          this.quizStatus = 'Quiz Summary';
        });
      }
    }, 1000);
  }

  closePopup() {
    $('.quizModal').modal('hide');
  }
}
