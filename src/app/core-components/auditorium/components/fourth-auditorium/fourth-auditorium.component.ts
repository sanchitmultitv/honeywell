import { Component, OnInit } from '@angular/core';
import { GroupChatFourComponent } from '../../../../layout/group-chat-four/group-chat-four.component';
declare var $:any;
@Component({
  selector: 'app-fourth-auditorium',
  templateUrl: './fourth-auditorium.component.html',
  styleUrls: ['./fourth-auditorium.component.scss'],
  providers:[GroupChatFourComponent]
})
export class FourthAuditoriumComponent implements OnInit {
  
  videoEnd = false;
  videoPlayerFour = 'https://dhzjgfv9krlhb.cloudfront.net/abr/smil:stream3.smil/playlist.m3u8';
  
  constructor(private gc:GroupChatFourComponent) { }

  ngOnInit(): void {
  }
  videoEnded() {
    this.videoEnd = true;
  }
  playAudioClap(){
    let playaudio : any= document.getElementById('myAudioClap');
    playaudio.play();
  }
  playAudioWhistle(){
    let playaudio : any= document.getElementById('myAudioWhistle');
    playaudio.play();
  }
  openGroupChat(){
    $('.groupchatsModalFour').modal('show')
    this.gc.loadData();
  }
}
