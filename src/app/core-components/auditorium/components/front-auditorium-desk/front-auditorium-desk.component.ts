import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FetchDataService } from 'src/app/services/fetch-data.service';
declare var $:any;
@Component({
  selector: 'app-front-auditorium-desk',
  templateUrl: './front-auditorium-desk.component.html',
  styleUrls: ['./front-auditorium-desk.component.scss']
})
export class FrontAuditoriumDeskComponent implements OnInit {
  actives:any=[];
  constructor(private router: Router, private _fd:FetchDataService) { }

  ngOnInit(): void {
    this.audiActive();
  }
  audiActive(){
    this._fd.activeAudi().subscribe(res=>{
      this.actives = res.result;
    })
  }
  gotoAuditoriumFront(){
    if(this.actives[1].status == true){
      this.router.navigate(['/auditorium/three']);
    }
   else{
    $('.audiModal').modal('show');
   }
  }
  gotoAuditoriumFourth(){
    if(this.actives[0].status == true){
      this.router.navigate(['/auditorium/four']);
    }
    else{
      $('.audiModal').modal('show');
     }
  }
}
