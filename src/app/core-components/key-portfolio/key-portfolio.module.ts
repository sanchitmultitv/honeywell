import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { KeyPortfolioRoutingModule } from './key-portfolio-routing.module';
import { KeyPortfolioComponent } from './key-portfolio/key-portfolio.component';


@NgModule({
  declarations: [KeyPortfolioComponent],
  imports: [
    CommonModule,
    KeyPortfolioRoutingModule
  ]
})
export class KeyPortfolioModule { }
