import { Component, OnInit } from '@angular/core';
import { ChatService } from 'src/app/services/chat.service';
declare var $: any;
@Component({
  selector: 'app-key-portfolio',
  templateUrl: './key-portfolio.component.html',
  styleUrls: ['./key-portfolio.component.scss']
})
export class KeyPortfolioComponent implements OnInit {
  player;
  videoName;
  liveMsg=false;
  document=[
    {title:'Dr Indira', url:'assets/lilly/scientfic_posters/01Dr_Indira.pdf'},
    {title:'poster presentation', url:'assets/lilly/scientfic_posters/02_poster_presentation.pdf'},
    {title:'Dr Ibrahim', url:'assets/lilly/scientfic_posters/03Dr_Ibrahim.pdf'},
    {title:'Case Report', url:'assets/lilly/scientfic_posters/04_Case_Report.pdf'},
    {title:'05 POSTER', url:'assets/lilly/scientfic_posters/05POSTER.pdf'},
    {title:'Dr Anil Shah', url:'assets/lilly/scientfic_posters/06Dr_Anil-Shah.pdf'},
    {title:'Anil Yadav PosterACDB', url:'assets/lilly/scientfic_posters/07Anil_Yadav-PosterACDB.pdf'},
    {title:'Dr Husain Ahmad', url:'assets/lilly/scientfic_posters/08Dr_Husain_Ahmad.pdf'},
    {title:'Poster Hemiballismus', url:'assets/lilly/scientfic_posters/09Poster_Hemiballismus.pdf'},

    {title:'10 Poster', url:'assets/lilly/scientfic_posters/10Poster.pdf'},
    {title:'11 POSTER', url:'assets/lilly/scientfic_posters/11POSTER.pdf'},
  ];
  constructor(private chat: ChatService) { }

  ngOnInit(): void {
    this.chat.getconnect('toujeo-60');
    this.chat.getMessages().subscribe((data=>{
    //  console.log('data',data);
      if(data == 'start_live'){
        this.liveMsg = true;
      }
      if(data == 'stop_live'){
        this.liveMsg = false;
      }
     
    }));
    let playVideo: any = document.getElementById("playVideo");
    window.onclick = (event) => {
      if (event.target == playVideo) {
        playVideo.style.display = "none";
        let pauseVideo: any = document.getElementById("video");
        pauseVideo.currentTime=0;
        pauseVideo.pause();
        console.log('event', event);
      }
    }
  }
  closePopupdocs(){
    $('.docsModal').modal('hide');
    $('#bikeRacing').modal('hide');
    $('#bubbleShoot').modal('hide');
  }
  playDiabetesVideo() {
    $('#playVideo').modal('show');
    this.player = 'assets/serdia_cardio/diabetes.mp4';
    this.videoName = 'Diabetes';
  }
  playCardiologyVideo() {
    $('#playVideo').modal('show');
    this.player = 'assets/serdia_cardio/hypertention.mp4';
    this.videoName = 'Cardiology';
  }
  playHypertensionVideo() {
    $('#playVideo').modal('show');
    this.player = 'assets/serdia_cardio/cardiology.mp4';
    this.videoName = 'Hypertension';
  }
  playVenousDiseaseVideo() {
    $('#playVideo').modal('show');
    this.player = '../../../../assets/serdia_myanmar/videos/key_portfolio/Venous disease.mp4';
    this.videoName = 'VenousDisease';
  }
  closePopup() {
    let pauseVideo: any = document.getElementById("video");
    pauseVideo.pause();
    $('#playVideo').modal('hide');
  }
}
