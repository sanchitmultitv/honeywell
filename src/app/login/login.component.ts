import { Component, OnInit, HostListener } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';

import { Router } from '@angular/router';
import { FetchDataService } from '../services/fetch-data.service';
import { AuthService } from '../services/auth.service';
function emailDomainValidator(control: FormControl) {
  let email = control.value;
  if (email && email.indexOf("@") != -1) {
    let [_, domain] = email.split("@");
    if (domain !== "sanofi.com" && domain !== "sanofi-india.com") {
      return {
        emailDomain: {
          parsedDomain: domain
        }
      }
    }
  }
  return null;
}

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  token;
  msg;
  //loginForm:FormGroup
  profileForm = new FormGroup({
    name: new FormControl(''),
    email: new FormControl(''),
    pass: new FormControl(''),
  });
  coverImage = "../../assets/img/h-about.jpg";
  videoPlay = false;
  potrait = false;
  // btnn= false;
  // text=true;
  constructor(private router: Router, private _fd: FetchDataService, private auth: AuthService, private formBuilder:FormBuilder) { }

  ngOnInit(): void {
    localStorage.setItem('user_guide', 'start');
    // this.loginForm = this.formBuilder.group({
    //   email: ['',[Validators.email,
    //    Validators.pattern("[^ @]*@[^ @]*"),
    //       emailDomainValidator]]
    // });
    // let hrs: any = new Date().getHours();
    // let mns: any = new Date().getMinutes();
    // let secs: any = new Date().getSeconds();
    //   if (hrs < 10) {
    //     hrs = '0' + hrs;
    //   }
    //   if (mns < 10) {
    //     mns = '0' + mns;
    //   }
    //   if (secs < 10) {
    //     secs = '0' + secs;
    //   }
    //   let timer = hrs+':'+mns+':'+secs;
    //   if (timer < '06:00:00') {
        // this.btnn = true;
        // this.text = false;
      // }else{
        // this.router.navigate(['/exhibitionHall']);
        // this.btnn = false;
        // this.text = true;
      // }

    if (window.innerHeight > window.innerWidth) {
      this.potrait = true;
    } else{
      this.potrait = false;
    }

    
  }

  loggedIn() {
    console.log('logindata', this.profileForm.value);
    // if(this.profileForm.value.pass == '4ACEDB'){
      // const user = {
      //   name : this.profileForm.value.name,
      //   email: this.profileForm.value.email,
      //   pass : this.profileForm.value.pass,
      //  // password: this.loginForm.get('password').value,
      //   event_id: 177,
      //   role_id: 1
      // };
      const formData = new FormData();
      // formData.append('event_id', '177');
      // formData.append('name', this.profileForm.value.name);
      formData.append('email', this.profileForm.value.email);
     // formData.append('headquarter', this.signupForm.get('job_title').value);
      var isMobile = {
        Android: function() {
            return navigator.userAgent.match(/Android/i);
        },
        BlackBerry: function() {
            return navigator.userAgent.match(/BlackBerry/i);
        },
        iOS: function() {
            return navigator.userAgent.match(/iPhone|iPad|iPod/i);
        },
        Opera: function() {
            return navigator.userAgent.match(/Opera Mini/i);
        },
        Windows: function() {
            return navigator.userAgent.match(/IEMobile/i) || navigator.userAgent.match(/WPDesktop/i);
        },
        any: function() {
            return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
        }
        
    };
    
     // console.log("password",user.password)
      this.auth.loginMethod(this.profileForm.value.email).subscribe((res: any) => {
        if (res.code === 1) {
          if( isMobile.iOS() ){
            this.videoPlay = false;
            this.router.navigateByUrl('/lobby');
          }
          this.videoPlay = true;
          localStorage.setItem('virtual', JSON.stringify(res.result));
          this.videoPlay = true;
          let vid: any = document.getElementById('myVideo');
          vid.play();
          if (window.innerHeight>window.innerWidth){
            this.potrait = true;
          }else{
            this.potrait = false;
          }
        } else {
          this.msg = 'Invalid Login';
          this.videoPlay = false;
          this.profileForm.reset();
        }
      }, (err: any) => {
        this.videoPlay = false;
        console.log('error', err)
      });
  this.profileForm.reset();

    }
  // else{
  //   alert('Please enter correct password');
  // }
    // this._fd.authLogin(user).subscribe(res => {
    //   if (res.code === 1) {
    //     this.videoPlay = true;
    //     localStorage.setItem('virtual', JSON.stringify(res.data));
    //     // this.router.navigate(['/lobby']);
    //     this.videoPlay = true;
    //     let vid: any = document.getElementById('myVideo');
    //     vid.play();
    //   } else {
    //     this.msg = 'Invalid Login';
    //     this.videoPlay = false;
    //     this.loginForm.reset();
    //   }
    // }, err => {
    //   this.videoPlay = false;
    //   console.log('error', err)
    // });
  // }
  @HostListener('window:resize', ['$event']) onResize(event) {
    if (window.innerHeight > window.innerWidth) {
      this.potrait = true;
    } else{
      this.potrait = false;
    }
  }
  
  endVideo() {
    // this.videoPlay = false;
    // this.potrait = false;
    this.router.navigateByUrl('/lobby');
    let welcomeAuido:any = document.getElementById('myAudio');
    welcomeAuido.play();
  }
}
